from shutil import copyfile

import pandas as pd

df = pd.read_csv('data/data_hpc2019/listado_procesado_geocode_hpc_delimited_prep_bim_audio.csv')

for index, row in df.iterrows():
    src = 'data/bim/'
    dst = 'data/data_hpc2019/bim_delimited/'
    copyfile(src + row['data_file'], dst + row['data_file'])
