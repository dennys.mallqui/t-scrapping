import json
import os
from os import listdir
from os.path import isfile, join
from urllib.parse import urlparse

import pandas as pd
from pandas.io.json import json_normalize

path = 'data/data_hpc2019/bim_delimited/'

onlyfiles = [f for f in listdir(path) if
             isfile(join(path, f))]

df_result = False
first = True

for f in onlyfiles:
    with open(path + f, 'r', encoding='utf8', errors='ignore') as file:
        data = file.read()
        json_data = json.loads(data)
        df = json_normalize(json_data['log']['entries'][0]['response'], record_path=None, meta=None,
                            meta_prefix=None, record_prefix=None, errors='ignore', sep='.')
        df['filename'] = f
        df['nro'] = int(f.replace('.json', ''))
        if first == True:
            first = False
            df_result = df
        else:
            df_result = pd.concat([df_result, df])

mylist = list()

for index, row in df_result.iterrows():
    try:
        from BeautifulSoup import BeautifulSoup
    except ImportError:
        from bs4 import BeautifulSoup
    html = row['content.text']
    parsed_html = BeautifulSoup(html, features="lxml")
    row['type_arch'] = str(
        parsed_html.body.find('span', attrs={'id': 'svfrmExpMdlDerivarRZ1Credito:formDatGen:cmbTipoArquitectura'}).text)
    row['cron'] = str(parsed_html.body.find('span',
                                            attrs={
                                                'id': 'svfrmExpMdlDerivarRZ1Credito:formDatGen:cmbFiliacionCronologica'}).text)
    data_url = ''
    data_file = ''
    for c in range(4):
        try:
            id = 'svfrmExpMdlDerivarRZ1Credito:formDatGen:dteDatosCondicionCultural:' + str(
                c) + ':colbtnVerAccionAdjuntoNorma'


            url = parsed_html.body.find('a', attrs={'id': id})['href']
            a = urlparse(url)
            if data_url == '':
                data_url = url
                data_file = os.path.basename(a.path)
            else:
                data_url = url + '|' + data_url
                data_file = os.path.basename(a.path) + '|' + data_file
        except:
            pass

    row['data_url'] = data_url
    row['data_file'] = data_file
    mylist.append(row)

df_result_1 = pd.DataFrame(mylist)
df_result_1 = df_result_1[['nro', 'type_arch', 'cron', 'data_url', 'data_file', 'filename']]

df_result_1 = pd.DataFrame(df_result_1)

df = pd.read_csv('data/data_hpc2019/listado_procesado_geocode_hpc_delimited_prep.csv')
df = pd.merge(df, df_result_1, on='nro')
df.to_csv('data/data_hpc2019/listado_procesado_geocode_hpc_delimited_prep_bim.csv', index=False)
print(df.columns)
