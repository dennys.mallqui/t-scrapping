import pandas as pd
import plotly
import plotly.graph_objs as go
import plotly.plotly as py

df = pd.read_csv('data/data_hpc2019/listado_procesado_geocode_hpc_total_delimited_ubi_prep.csv')

lat = 0
long = 0

for index, row in df.iterrows():
    lat += row['latitud']
    long += row['longitud']

lat = lat / df.shape[0]
long = long / df.shape[0]

plotly.tools.set_credentials_file(username='m3g4r00t', api_key='Y82XHERTU93N9zSObIWr')

mapbox_access_token = 'pk.eyJ1IjoibTNnNHIwMHQiLCJhIjoiY2p3NGp1dWlwMTFybjN5bzlxdm05b2I1ciJ9.M0I0EBCQXLmTtu53Qcz91g'

data = [
    go.Scattermapbox(
        lat=[lat],
        lon=[long],
        mode='markers',
        marker=go.scattermapbox.Marker(
            size=14
        ),
        text=['Centro Histórico de Lima'],
    )
]

layout = go.Layout(
    autosize=True,
    hovermode='closest',
    mapbox=go.layout.Mapbox(
        accesstoken=mapbox_access_token,
        bearing=0,
        center=go.layout.mapbox.Center(
            lat=lat,
            lon=long
        ),
        pitch=0,
        zoom=14
    ),
)

for index, row in df.iterrows():
    point = go.Scattermapbox(
        lat=[row['latitud']],
        lon=[row['longitud']],
        mode='markers',
        marker=go.scattermapbox.Marker(
            size=14
        ),
        name=row['nro'],
        text=[str(row['nombre']) + '</br>' + str(row['formatted_address'])],
    )
    data.append(point)

trace1 = go.Scattermapbox(
    lat=[-12.043575, -12.045333, -12.048262, -12.046496, -12.043575],
    lon=[-77.031049, -77.027988, -77.030088, -77.033075, -77.031049],
    mode='lines',
    name='Plaza Mayor',
    text=['Plaza Mayor'],
)

data.append(trace1)
fig = go.Figure(data=data, layout=layout)

url = py.plot(fig, filename='Centro Histórico de Lima Mapbox')
