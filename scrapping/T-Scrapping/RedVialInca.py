import time
from urllib.request import urlopen

url = 'http://sigda.cultura.gob.pe/arcgis/rest/services/sigda_v3/BASE_GRAFICA_DSFL/MapServer/find?searchText=.&contains=true&searchFields=&sr=&layers=3&layerDefs=&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&dynamicLayers=&returnZ=false&returnM=false&gdbVersion=&f=json'
file = "data/text/red-vial-inca-" + time.strftime("%Y%m%d-%H%M%S") + ".json"
f = open(file, "w+", encoding='utf8')
f.write(urlopen(url).read().decode("utf8", "ignore"))
f.close()
