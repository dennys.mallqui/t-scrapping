import json

from pandas.io.json import json_normalize

filename = 'data/text/arq-declarado-20190602-115640.json'

with open(filename, 'r', encoding='utf8', errors='ignore') as file:
    data = file.read()
    json_data = json.loads(data)
    df = json_normalize(json_data, record_path=['layers', 'features'])
    df.to_csv('data/data_hpc2019/arq-declarado-20190602-115640.csv', index=False)

filename = 'data/text/arq-delimitados-20190602-115731.json'

with open(filename, 'r', encoding='utf8', errors='ignore') as file:
    data = file.read()
    json_data = json.loads(data)
    df = json_normalize(json_data, record_path=['layers', 'features'])
    df.to_csv('data/data_hpc2019/arq-delimitados-20190602-115731.csv', index=False)

filename = 'data/text/arq-referencial-inventario-20190602-114231.json'

with open(filename, 'r', encoding='utf8', errors='ignore') as file:
    data = file.read()
    json_data = json.loads(data)
    df = json_normalize(json_data, record_path=['layers', 'features'])
    df.to_csv('data/data_hpc2019/arq-referencial-inventario-20190602-114231.csv', index=False)

filename = 'data/text/arq-referencial-registrado-20190602-115550.json'

with open(filename, 'r', encoding='utf8', errors='ignore') as file:
    data = file.read()
    json_data = json.loads(data)
    df = json_normalize(json_data, record_path=['layers', 'features'])
    df.to_csv('data/data_hpc2019/arq-referencial-registrado-20190602-115550.csv', index=False)

filename = 'data/text/red-vial-inca-20190602-120856.json'

with open(filename, 'r', encoding='utf8', errors='ignore') as file:
    data = file.read()
    json_data = json.loads(data)
    df = json_normalize(json_data, record_path=['results'])
    df.to_csv('data/data_hpc2019/red-vial-inca-20190602-120856.csv', index=False)

filename = 'data/text/museos-20190602-113538.json'

with open(filename, 'r', encoding='utf8', errors='ignore') as file:
    data = file.read()
    json_data = json.loads(data)
    df = json_normalize(json_data, record_path=['results'])
    df.to_csv('data/data_hpc2019/museos-20190602-113538.csv', index=False)