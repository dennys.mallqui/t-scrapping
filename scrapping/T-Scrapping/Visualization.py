import gmplot
import pandas as pd

# GoogleMapPlotter return Map object
# Pass the center latitude and
# center longitude

df = pd.read_csv('data/data_hpc2019/listado_procesado_geocode_hpc_total_delimited_ubi_prep.csv')

lat = 0
long = 0

for index, row in df.iterrows():
    lat += row['latitud']
    long += row['longitud']

lat = lat / df.shape[0]
long = long / df.shape[0]

gmap1 = gmplot.GoogleMapPlotter(lat,
                                long, 13)

# Pass the absolute path
gmap1.draw("data/data_hpc2019/maps/map11.html")

import webbrowser

a_website = "C:/Users/Dennys/Documents/HPC2019/tucuiricuc/scrapping/T-Scrapping/data/data_hpc2019/maps/map11.html"

# Open url in a new window of the default browser, if possible
webbrowser.open_new(a_website)

