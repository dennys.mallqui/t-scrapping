import os

import requests
from bs4 import BeautifulSoup

url = 'http://repositorioarchivos.cultura.gob.pe/bim_files/anexos/'


def listFD(url):
    page = requests.get(url).text
    soup = BeautifulSoup(page, 'html.parser')
    return [url + node.get('href') for node in soup.find_all('a')]


def downloadFileIfNotExist(url, dest):
    """Downloads URL if file does not already exist.
    Args:
        url (str): URL to download.
    Returns:
        tuple: Tuple containing:
            * fnDL (str): Donwload filename
            * folderFn (str): Filename of extracted download files
    """

    import wget

    cwd = dest + "/"
    if not os.path.exists(cwd + os.path.basename(url)):
        print(cwd + os.path.basename(url) + " does not exist, will download it.")
        folderFn = wget.download(url, out=dest)
    else:
        print(cwd + os.path.basename(url) + " alreay exists, will not download.")

    return True


for file in listFD(url):
    filename = file.split('/')[len(file.split('/')) - 1]
    downloadFileIfNotExist(url + filename, 'data/bim')
