import pandas as pd

xl_file = pd.ExcelFile('data/data_hpc/inmuebles.xlsx')

dfs = {sheet_name: xl_file.parse(sheet_name)
       for sheet_name in xl_file.sheet_names}
df = dfs['Inmuebles Lima']
df_cat = dfs['Categorías']

df['nombre'] = df['DESCRIPCIÓN']
df.drop(['DESCRIPCIÓN'], axis=1, inplace=True)

df['direccion'] = df['DIRECCIÓN']
df.drop(['DIRECCIÓN'], axis=1, inplace=True)

df = pd.merge(df, df_cat, on=['COD. CATEGORÍA'])

df['categoria'] = df['NOMBRE']

df_1 = pd.read_csv('data/data_hpc2019/listado_procesado_geocode.csv')

df.replace('^\s+', '', regex=True, inplace=True)  # front
df.replace('\s+$', '', regex=True, inplace=True)  # end
df_1.replace('^\s+', '', regex=True, inplace=True)  # front
df_1.replace('\s+$', '', regex=True, inplace=True)  # end

result = pd.merge(df_1, df, on=['nombre', 'direccion'])

result.to_csv('data/data_hpc2019/listado_procesado_geocode_hpc.csv', index=False)
