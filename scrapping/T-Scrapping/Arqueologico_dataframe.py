import json

import numpy as np
import pandas as pd
from codecs import raw_unicode_escape_decode

df = pd.read_csv('data/data_hpc2019/arq-declarado-20190602-115640.csv', na_values='')
df = df.replace(np.nan, "''", regex=True)

mylist_1 = list()

for index, row in df.iterrows():
    j = json.loads(row['attributes'].replace("'", '"').replace("None", '"None"'))
    j2 = json.loads(row['geometry'].replace("'", '"').replace("None", '"None"'))
    j.update(j2)
    mylist_1.append(j)

df = pd.DataFrame(mylist_1)
df.to_csv('data/data_hpc2019/arq-declarado-20190602-115640-geocode.csv', index=False)

df = pd.read_csv('data/data_hpc2019/arq-delimitados-20190602-115731.csv', na_values='')
df = df.replace(np.nan, "''", regex=True)

mylist_1 = list()

for index, row in df.iterrows():
    print(raw_unicode_escape_decode(row['attributes'].replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace("': ",
                                                                                                                  '": ').replace(
            ": '", ': "').replace("', ",
                                  '", ').replace(
            "None", '"None"'))[0])
    j = json.loads(
        raw_unicode_escape_decode(row['attributes'].replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace("': ",
                                                                                                                  '": ').replace(
            ": '", ': "').replace("', ",
                                  '", ').replace(
            "None", '"None"'))[0], encoding='cp1252')
    j2 = json.loads(
        raw_unicode_escape_decode(row['geometry'].replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace("': ",
                                                                                                                '": ').replace(
            ": '", ': "').replace("', ",
                                  '", ').replace(
            "None", '"None"'))[0], encoding='cp1252')
    j.update(j2)
    mylist_1.append(j)

df = pd.DataFrame(mylist_1)
df.to_csv('data/data_hpc2019/arq-delimitados-20190602-115731-geocode.csv', index=False)

df = pd.read_csv('data/data_hpc2019/arq-referencial-inventario-20190602-114231.csv', na_values='')
df = df.replace(np.nan, "''", regex=True)

mylist_1 = list()

for index, row in df.iterrows():
    print(raw_unicode_escape_decode(
        row['attributes'].replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace("': ",
                                                                                                                  '": ').replace(
            ": '", ': "').replace("', ",
                                  '", ').replace(
            "None", '"None"'))[0])
    j = json.loads(
        raw_unicode_escape_decode(
            row['attributes'].replace('\\', "/").replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace(
                "': ",
                '": ').replace(
                ": '", ': "').replace("', ",
                                      '", ').replace(
                "None", '"None"'))[0], encoding='cp1252')
    j2 = json.loads(
        raw_unicode_escape_decode(
            row['geometry'].replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace(
                "': ",
                '": ').replace(
                ": '", ': "').replace("', ",
                                      '", ').replace(
                "None", '"None"'))[0], encoding='cp1252')
    j.update(j2)
    mylist_1.append(j)

df = pd.DataFrame(mylist_1)
df.to_csv('data/data_hpc2019/arq-referencial-inventario-20190602-114231-geocode.csv', index=False)

df = pd.read_csv('data/data_hpc2019/arq-referencial-registrado-20190602-115550.csv', na_values='')
df = df.replace(np.nan, "''", regex=True)

mylist_1 = list()

for index, row in df.iterrows():
    try:
        print(raw_unicode_escape_decode(
                row['attributes'].replace('\\', "/").replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace(
                    "': ",
                    '": ').replace(
                    ": '", ': "').replace("', ",
                                          '", ').replace(
                    "None", '"None"').replace('"EL', "'EL").replace('CAMINO"', "CAMINO'"))[0])
        j = json.loads(
            raw_unicode_escape_decode(
                row['attributes'].replace('\\', "/").replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace(
                    "': ",
                    '": ').replace(
                    ": '", ': "').replace("', ",
                                          '", ').replace(
                    "None", '"None"').replace('"EL', "'EL").replace('CAMINO"', "CAMINO'"))[0], encoding='cp1252')
        j2 = json.loads(
            raw_unicode_escape_decode(
                row['geometry'].replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace(
                    "': ",
                    '": ').replace(
                    ": '", ': "').replace("', ",
                                          '", ').replace(
                    "None", '"None"'))[0], encoding='cp1252')
        j.update(j2)
        mylist_1.append(j)
    except:
        pass

df = pd.DataFrame(mylist_1)
df.to_csv('data/data_hpc2019/arq-referencial-registrado-20190602-115550-geocode.csv', index=False)

df = pd.read_csv('data/data_hpc2019/museos-20190602-113538.csv', na_values='')
df = df.replace(np.nan, "''", regex=True)

mylist_1 = list()

for index, row in df.iterrows():
    try:
        j = json.loads(
            row['attributes'].replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace("': ",
                                                                                                                      '": ').replace(
                ": '", ': "').replace("', ",
                                      '", ').replace(
                "None", '"None"'), encoding='cp1252')
        row.pop('attributes')
        j.update(row)
        mylist_1.append(j)
    except:
        pass

df = pd.DataFrame(mylist_1)
df.to_csv('data/data_hpc2019/museos-20190602-113538-geocode.csv', index=False)

df = pd.read_csv('data/data_hpc2019/red-vial-inca-20190602-120856.csv', na_values='')
df = df.replace(np.nan, "''", regex=True)

mylist_1 = list()

for index, row in df.iterrows():
    j = json.loads(
        row['attributes'].replace('"', "'").replace("{'", '{"').replace("'}", '"}').replace(", '", ', "').replace("': ",
                                                                                                                  '": ').replace(
            ": '", ': "').replace("', ",
                                  '", ').replace(
            "None", '"None"'), encoding='cp1252')
    row.pop('attributes')
    j.update(row)
    mylist_1.append(j)

df = pd.DataFrame(mylist_1)
df.to_csv('data/data_hpc2019/red-vial-inca-20190602-120856-geocode.csv', index=False)
