from shapely.geometry import Point, Polygon

point = Point(-12.045562, -77.030972)

coords = ((-12.043575, -77.031049), (-12.045333, -77.027988), (-12.048262, -77.030088), (-12.046496, -77.033075))
polygon = Polygon(coords)

print('Resultado: ', polygon.contains(point))

