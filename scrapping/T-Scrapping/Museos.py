import time
from urllib.request import urlopen

url = 'http://sigda.cultura.gob.pe/arcgis/rest/services/sigda/Museo/MapServer/find?searchText=.&layers=0&f=json'
file = "data/text/museos-" + time.strftime("%Y%m%d-%H%M%S") + ".json"
f = open(file, "w+", encoding='utf8')
f.write(urlopen(url).read().decode("utf8", "ignore"))
f.close()
