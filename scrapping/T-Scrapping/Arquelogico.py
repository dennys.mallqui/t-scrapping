import time
from urllib.request import urlopen

url = 'http://sigda.cultura.gob.pe/arcgis/rest/services/sigda_v3/sigda_v3_invitado/FeatureServer/query?layerDefs=0&f=pjson'
file = "data/text/arq-referencial-inventario-" + time.strftime("%Y%m%d-%H%M%S") + ".json"
f = open(file, "w+", encoding='utf8')
f.write(urlopen(url).read().decode("utf8", "ignore"))
f.close()

url = 'http://sigda.cultura.gob.pe/arcgis/rest/services/sigda_v3/sigda_v3_invitado/FeatureServer/query?layerDefs=1&f=pjson'
file = "data/text/arq-referencial-registrado-" + time.strftime("%Y%m%d-%H%M%S") + ".json"
f = open(file, "w+", encoding='utf8')
f.write(urlopen(url).read().decode("utf8", "ignore"))
f.close()

url = 'http://sigda.cultura.gob.pe/arcgis/rest/services/sigda_v3/sigda_v3_invitado/FeatureServer/query?layerDefs=2&f=pjson'
file = "data/text/arq-declarado-" + time.strftime("%Y%m%d-%H%M%S") + ".json"
f = open(file, "w+", encoding='utf8')
f.write(urlopen(url).read().decode("utf8", "ignore"))
f.close()

url = 'http://sigda.cultura.gob.pe/arcgis/rest/services/sigda_v3/sigda_v3_invitado/FeatureServer/query?layerDefs=3&f=pjson'
file = "data/text/arq-delimitados-" + time.strftime("%Y%m%d-%H%M%S") + ".json"
f = open(file, "w+", encoding='utf8')
f.write(urlopen(url).read().decode("utf8", "ignore"))
f.close()
