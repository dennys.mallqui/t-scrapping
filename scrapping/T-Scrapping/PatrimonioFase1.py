import json

import pandas as pd

filename = 'data/patrimonio/listado.json'

with open(filename, 'r', encoding='utf8', errors='ignore') as file:
    data = file.read()
    json_data = json.loads(data, encoding='utf8')
    for key, value in json_data.items():
        for key2, value2 in value.items():
            if key2 == 'entries':
                count = 0
                pat = list()
                for i in value2:
                    if 'response' in str(i):
                        if 'text' in str(i):
                            dfs = pd.read_html(str(i))
                            # print(dfs[1].head())
                            df = pd.DataFrame(dfs[0][['Nro', 'Categoría', 'Nombre del Bien Inmueble', 'Dirección',
                                                      'Base Legal', 'Unnamed: 9']])
                            df = pd.DataFrame(df[df['Base Legal'] == 'Ver Detalle'])
                            # print(df.head())
                            pat.append(df)

pat = pd.concat(pat, ignore_index=True)
pat = pat[['Nro', 'Categoría', 'Nombre del Bien Inmueble', 'Dirección']]
pat.columns = ['nro','categoria','nombre','direccion']
pat.to_csv('data/patrimonio/listado_procesado.csv', index=False)
