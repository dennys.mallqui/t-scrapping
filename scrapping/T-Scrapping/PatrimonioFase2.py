import googlemaps
import pandas as pd
from pandas.io.json import json_normalize

gmaps = googlemaps.Client(key='AIzaSyCAkekJUAp-Ndyqd6uR_ruJsbNF71wy8Vw')
filename = 'data/patrimonio/listado_procesado.csv'
pat = pd.read_csv(filename)
result = list()

for index, row in pat.iterrows():
    # Geocoding an address
    geocode_result1 = gmaps.geocode(str(row['nombre']) + ', Lima')
    df = json_normalize(geocode_result1)
    for index2, row2 in df.iterrows():
        row = row.append(row2)
        break

    geocode_result2 = gmaps.geocode(str(row['direccion']) + ', Lima')
    df = json_normalize(geocode_result2)
    df.rename(str.upper, axis='columns', inplace=True)
    for index2, row2 in df.iterrows():
        row = row.append(row2)
        break

    result.append(row)


filename = 'data/patrimonio/listado_procesado_geocode.csv'
result = pd.DataFrame(result)
result.to_csv(filename, index=False)
