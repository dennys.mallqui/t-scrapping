import pandas as pd

xl_file = pd.ExcelFile('data/data_hpc/inmuebles.xlsx')

dfs = {sheet_name: xl_file.parse(sheet_name)
       for sheet_name in xl_file.sheet_names}
df = dfs['Inmuebles Lima']
df_cat = dfs['Categorías']

df['nombre'] = df['DESCRIPCIÓN']
df.drop(['DESCRIPCIÓN'], axis=1, inplace=True)

df['direccion'] = df['DIRECCIÓN']
df.drop(['DIRECCIÓN'], axis=1, inplace=True)

df = pd.merge(df, df_cat, on=['COD. CATEGORÍA'])

df['categoria'] = df['NOMBRE']

df_1 = pd.read_csv('data/data_hpc2019/listado_procesado_geocode.csv')

df.replace('^\s+', '', regex=True, inplace=True)  # front
df.replace('\s+$', '', regex=True, inplace=True)  # end
df_1.replace('^\s+', '', regex=True, inplace=True)  # front
df_1.replace('\s+$', '', regex=True, inplace=True)  # end

result = pd.merge(df_1, df, on=['categoria', 'direccion'])

result.to_csv('data/data_hpc2019/listado_procesado_geocode_hpc_total.csv', index=False)

xl_file = pd.ExcelFile('data/data_hpc/ubigeos.xlsx')

dfs = {sheet_name: xl_file.parse(sheet_name)
       for sheet_name in xl_file.sheet_names}

df = dfs['UBIGEOS']

result = pd.merge(result, df, on=['COD. DISTRITO'])

result.to_csv('data/data_hpc2019/listado_procesado_geocode_hpc_total_ubi.csv', index=False)

df_perimeter = pd.read_csv('data/data_hpc2019/delimiters_2.csv')

import json
from shapely.geometry import Point, Polygon

for index, row in df_perimeter.iterrows():
    coords = ((row['x1'], row['y1']), (row['x2'], row['y2']), (row['x3'], row['y3']), (row['x4'], row['y4']))

polygon = Polygon(coords)

mylist = list()
for index, row in result.iterrows():
    point = Point(row['geometry.location.lat'], row['geometry.location.lng'])
    if polygon.contains(point):
        row['delimited_by'] = 'geometry.location'
        mylist.append(row)
    else:
        point = Point(row['GEOMETRY.LOCATION.LAT'], row['GEOMETRY.LOCATION.LNG'])
        if polygon.contains(point):
            row['delimited_by'] = 'GEOMETRY.LOCATION'
            mylist.append(row)

df = pd.DataFrame(mylist)

df.to_csv('data/data_hpc2019/listado_procesado_geocode_hpc_total_delimited_ubi.csv', index=False)

