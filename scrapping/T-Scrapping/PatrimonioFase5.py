import pandas as pd

df = pd.read_csv('data/data_hpc2019/listado_procesado_geocode_hpc_total_delimited_ubi.csv')

mylist = list()

for index, row in df.iterrows():
    newrow = {}
    newrow['nro'] = int(row['nro'])
    newrow['nombre'] = row['nombre_y']
    newrow['direccion'] = row['direccion']
    newrow['categoria'] = row['categoria']
    newrow['tipo_norma'] = row['TIPO DE NORMA']
    newrow['num_norma'] = row['NÚM. DE NORMA']
    newrow['archivo_norma'] = row['ARCHIVO DE NORMA']
    newrow['cod_departamento'] = row['COD. DEPARTAMENTO']
    newrow['departamento'] = row['DEPARTAMENTO']
    newrow['cod_provincia'] = row['COD. PROVINCIA']
    newrow['provincia'] = row['PROVINCIA']
    newrow['distrito'] = row['DISTRITO']
    if row['delimited_by'] == 'geometry.location':
        newrow['latitud'] = row['geometry.location.lat']
        newrow['longitud'] = row['geometry.location.lng']
        newrow['place_id'] = row['place_id']
        newrow['types'] = row['types']
        newrow['formatted_address'] = row['formatted_address']
    else:
        newrow['latitud'] = row['GEOMETRY.LOCATION.LAT']
        newrow['longitud'] = row['GEOMETRY.LOCATION.LNG']
        newrow['place_id'] = row['PLACE_ID']
        newrow['types'] = row['TYPES']
        newrow['formatted_address'] = row['FORMATTED_ADDRESS']
    mylist.append(newrow)

df = pd.DataFrame(mylist)

df.to_csv('data/data_hpc2019/listado_procesado_geocode_hpc_total_delimited_ubi_prep.csv', index=False)